import axios from "axios";

const { default: axios } = require("axios");

//https://restcountries.com/v3.1/all

const asyncFunct = async () => {
  const fetchData = await axios.get("https://restcountries.com/v3.1/all");
  console.log(fetchData);
};
asyncFunct();
