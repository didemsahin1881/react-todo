import React from "react";

const Signup = () => {
  const onSubmit = (event) => {
    console.log("submit button click");
  };
  return (
    <div>
      <div className="flex">
        <h2>Sign up</h2>
      </div>
      <form>
        <div className="form-group">
          <label>Username</label>
          <input id="username"></input>
        </div>
        <div className="form-group">
          <label>Password</label>
          <input id="password"></input>
        </div>
        <div className="form-group">
          <button onClick={(e) => onSubmit(e)}></button>
        </div>
      </form>
    </div>
  );
};
export default Signup;
