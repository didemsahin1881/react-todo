import React, { useState, useEffect } from "react";
import "./todo.css";

const Todo = () => {
  //countries axios get data https://restcountries.com/v3.1/all
  useEffect(() => {
    fetch("https://restcountries.com/v3.1/all")
      .then((response) => response.json)
      .then((response) => console.log(response));
  }, []);

  const [newItem, setNewItem] = useState("");
  const [Items, setItems] = useState([]);
  const [showError, setShowError] = useState(false);
  const [errorMessage, setErrorMessage] = useState();

  function addItem(e) {
    if (!newItem) {
      setShowError(true);
      setErrorMessage("Boş ekleme yapılamamaktadır.!");
      return;
    }
    const item = {
      ID: Math.floor(Math.random() * 1000),
      value: newItem,
    };
    setItems((oldItems) => [...oldItems, item]);
    setNewItem("");
    setShowError(false);
    setErrorMessage("");
  }
  const addItemEnterBtn = (event) => {
    if (event.keyCode === 13) {
      addItem(event);
    }
  };
  function deleteItem(Id) {
    const newArray = Items.filter((item) => item.ID !== Id);
    setItems(newArray);
  }

  function navigateSignup() {
    console.log("navigate signup");
  }
  return (
    <div className="App">
      <div className="signup">
        <button onClick={navigateSignup}>SignUp</button>
      </div>
      <header className="App-header">
        <div className="todo-container">
          <h1>Todo List App</h1>

          <div className="todo-input">
            <input
              type="text"
              value={newItem}
              onKeyDown={(e) => addItemEnterBtn(e)}
              onChange={(e) => setNewItem(e.target.value)}
              placeholder="add in item.."
            ></input>
          </div>
          <div className="todo-button">
            <button onClick={() => addItem()}>Add</button>
          </div>
          {showError && (
            <div className="alert alert-danger alert-flashMsg">
              {errorMessage}
              <button onClick={() => setShowError(false)}>X</button>
            </div>
          )}
        </div>

        <div className="todo-list">
          <ul>
            {Items.map((item) => {
              return (
                <li key={item.ID}>
                  {item.value}
                  <button onClick={() => deleteItem(item.ID)}>X</button>
                </li>
              );
            })}
          </ul>
        </div>
      </header>
    </div>
  );
};

export default Todo;
